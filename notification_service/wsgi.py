"""
WSGI config for notification_service project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
from apscheduler.schedulers.background import BackgroundScheduler

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "notification_service.settings")

application = get_wsgi_application()

scheduler = BackgroundScheduler()
