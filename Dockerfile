FROM python:3.9
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
COPY .env /code/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD ["python", "manage.py", "makemigrations"]
CMD ["python", "manage.py", "migrate"]
COPY . /code
