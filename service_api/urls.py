from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import ClientViewSet, MailingViewSet, MailingListStatisticsView, MailingDetailStatisticsView

router = DefaultRouter()
router.register(r'clients', ClientViewSet, basename='client')
router.register(r'mailings', MailingViewSet, basename='mailing')

urlpatterns = [
    path("mailings-list-statistic/", MailingListStatisticsView.as_view(), name="mailings-list-statistic"),
    path("mailings-detail-statistic/<int:pk>/", MailingDetailStatisticsView.as_view(), name="mailings-detail-statistic")
]

urlpatterns += router.urls
