from django.db import models
from django.db.models import Q

from .validators import phone_number_regex


class Mailing(models.Model):
    """Сущность Рассылка"""
    start_time = models.DateTimeField(verbose_name="Дата и время запуска рассылки")
    text = models.TextField(verbose_name="Текст сообщения для клиентов доставки")
    filters = models.JSONField(verbose_name="Свойства фильтра клиентов")
    stop_time = models.DateTimeField(verbose_name="Дата и время окончания рассылки")

    def __str__(self):
        return f"Текст рассылки: {self.text}"

    def all_messages_count(self):
        return self.messages.count()

    def sent_messages_count(self) -> int:
        return self.messages.filter(is_sent=True).count()

    def not_sent_messages_count(self) -> int:
        return self.messages.filter(is_sent=False).count()


class Client(models.Model):
    """Сущность Клиент"""
    phone_number = models.CharField(verbose_name="Номер телефона", validators=[phone_number_regex],
                                    max_length=11, unique=True)
    operator_code = models.PositiveIntegerField(verbose_name="Код оператора")
    tag = models.CharField(verbose_name="Производственная метка", max_length=25)
    time_zone = models.SmallIntegerField(verbose_name="Часовой пояс ("
                                                      "часы +- относительно Гринвича)")

    def __str__(self):
        return f"Номер телефона клиента: {self.phone_number}"


class Message(models.Model):
    """Сущность Сообщение"""
    time_create = models.DateTimeField(verbose_name="Время отправки сообщения", auto_now_add=True)
    is_sent = models.BooleanField(verbose_name="Статус сообщения", default=False)
    mailing = models.ForeignKey(to=Mailing, on_delete=models.CASCADE, verbose_name="Идентификатор рассылки",
                                related_name="messages")
    client = models.ForeignKey(to=Client, on_delete=models.CASCADE, verbose_name="Идентификатор клиента")

    def __str__(self):
        return f"Сообщение: \"{self.mailing.text}\" для клиента {self.client.phone_number}"
