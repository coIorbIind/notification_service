from django.core.validators import RegexValidator

phone_number_regex = RegexValidator(regex=r"^((\+7|7|8)+([0-9]){10})$")
