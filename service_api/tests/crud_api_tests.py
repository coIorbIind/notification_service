import json
from django.urls import reverse
from django.test import TestCase
from rest_framework import status

from service_api.models import Mailing, Client, Message
from service_api.serializers import MailingSerializer, ClientSerializer


class CRUDTests(TestCase):
    def setUp(self) -> None:
        self.mailing = Mailing.objects.create(
            start_time="2020-08-07T19:00",
            text="text",
            filters={
                "operator_code": 900
            },
            stop_time="2020-08-07T20:00"
        )

        self.client_1 = Client.objects.create(
            phone_number="79009001020",
            operator_code=900,
            tag="tag",
            time_zone=3,
        )

        self.client_2 = Client.objects.create(
            phone_number="79009001030",
            operator_code=900,
            tag="tag",
            time_zone=2,
        )

        self.message_1 = Message.objects.create(
            client=self.client_1,
            mailing=self.mailing
        )

        self.message_2 = Message.objects.create(
            is_sent=True,
            client=self.client_2,
            mailing=self.mailing
        )

    def test_client_list_get(self):
        url = reverse('client-list')
        response = self.client.get(url)

        expected_data = ClientSerializer([self.client_1, self.client_2], many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.data, expected_data)

    def test_client_detail_get(self):
        url = reverse('client-detail', args=(self.client_1.id,))
        response = self.client.get(url)

        expected_data = ClientSerializer(self.client_1).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.data, expected_data)

    def test_client_create(self):
        self.assertEqual(2, Client.objects.all().count())
        url = reverse('client-list')
        data = {
            "phone_number": "79009001040",
            "operator_code": 900,
            "tag": "tag",
            "time_zone": 1,
        }
        json_data = json.dumps(data)
        response = self.client.post(url, data=json_data, content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(3, Client.objects.all().count())
        self.assertEqual(response.data, ClientSerializer(Client.objects.last()).data)

    def test_client_update(self):
        data = {
            "phone_number": "79009001020",
            "operator_code": 900,
            "tag": "new_tag",
            "time_zone": 3,
        }
        json_data = json.dumps(data)
        url = reverse('client-detail', args=(self.client_1.id,))

        response = self.client.put(url, data=json_data, content_type='application/json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.client_1.refresh_from_db()
        self.assertEqual("new_tag", self.client_1.tag)

    def test_client_delete(self):
        self.assertEqual(2, Client.objects.all().count())
        url = reverse('client-detail', args=(self.client_2.id,))
        response = self.client.delete(url)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(1, Client.objects.all().count())

    def test_mailing_list_get(self):
        url = reverse('mailing-list')
        response = self.client.get(url)

        expected_data = MailingSerializer([self.mailing], many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.data, expected_data)

    def test_mailing_detail_get(self):
        url = reverse('mailing-detail', args=(self.mailing.id,))
        response = self.client.get(url)

        expected_data = MailingSerializer(self.mailing).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.data, expected_data)

    def test_mailing_update(self):
        data = {
            "id": self.mailing.id,
            "start_time": self.mailing.start_time,
            "text": "new text",
            "filters": self.mailing.filters,
            "stop_time": self.mailing.stop_time,
        }
        json_data = json.dumps(data)
        url = reverse('mailing-detail', args=(self.mailing.id,))

        response = self.client.put(url, data=json_data, content_type='application/json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.mailing.refresh_from_db()
        self.assertEqual("new text", self.mailing.text)

    def test_mailing_delete(self):
        self.assertEqual(1, Mailing.objects.all().count())
        url = reverse('mailing-detail', args=(self.mailing.id,))
        response = self.client.delete(url)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(0, Mailing.objects.all().count())

    def test_mailing_create(self):
        self.assertEqual(1, Mailing.objects.all().count())
        url = reverse('mailing-list')
        data = {
            "start_time": "2020-08-08T19:00",
            "text": "new text",
            "filters": {
                "operator_code": 910
            },
            "stop_time": "2020-08-08T20:00",

        }
        json_data = json.dumps(data)
        response = self.client.post(url, data=json_data, content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(2, Mailing.objects.all().count())
        self.assertEqual(response.data, MailingSerializer(Mailing.objects.last()).data)
