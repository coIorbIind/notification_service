from django.test import TestCase
from service_api.models import Mailing, Client, Message
from service_api.serializers import (MessageSerializer, ClientShortSerializer, MailingSerializer, ClientSerializer,
                                     MailingDetailStatisticSerializer, MailingListStatisticSerializer)


class SerializersTests(TestCase):
    def setUp(self) -> None:

        self.mailing = Mailing.objects.create(
            start_time="2020-08-07T19:00",
            text="text",
            filters={
                "operator_code": 900
            },
            stop_time="2020-08-07T20:00"
        )

        self.client = Client.objects.create(
            phone_number="79009001020",
            operator_code=900,
            tag="tag",
            time_zone=3,
        )

        self.message_1 = Message.objects.create(
            client=self.client,
            mailing=self.mailing
        )

        self.message_2 = Message.objects.create(
            is_sent=True,
            client=self.client,
            mailing=self.mailing
        )

    def test_client_short_serializer(self):
        expected_data = {
            "phone_number": self.client.phone_number
        }
        received_data = ClientShortSerializer(self.client).data
        self.assertEqual(received_data, expected_data)

    def test_message_serializer(self):
        expected_data = {
            'client': {
                "phone_number": self.client.phone_number
            },
            "is_sent": self.message_1.is_sent
        }
        received_data = MessageSerializer(self.message_1).data
        self.assertEqual(received_data, expected_data)

    def test_mailing_serializer(self):
        expected_data = {
            "id": self.mailing.id,
            "start_time": self.mailing.start_time,
            "text": self.mailing.text,
            "filters": self.mailing.filters,
            "stop_time": self.mailing.stop_time,
        }
        received_data = MailingSerializer(self.mailing).data
        self.assertEqual(received_data, expected_data)

    def test_client_serializer(self):
        expected_data = {
            "id": self.client.id,
            "phone_number": self.client.phone_number,
            "operator_code": self.client.operator_code,
            "tag": self.client.tag,
            "time_zone": self.client.time_zone,
        }
        received_data = ClientSerializer(self.client).data
        self.assertEqual(received_data, expected_data)

    def test_mailing_list_statistic_serializer(self):
        expected_data = {
            "text": self.mailing.text,
            "all_messages_count": 2,
            "sent_messages_count": 1,
            "not_sent_messages_count": 1
        }
        received_data = MailingListStatisticSerializer(self.mailing).data
        self.assertEqual(received_data, expected_data)

    def test_mailing_detail_statistic_serializer(self):
        expected_data = {
            "text": self.mailing.text,
            "all_messages_count": 2,
            "sent_messages_count": 1,
            "not_sent_messages_count": 1,
            "messages": MessageSerializer([self.message_1, self.message_2], many=True).data
        }
        received_data = MailingDetailStatisticSerializer(self.mailing).data
        print(expected_data)
        print(received_data)
        self.assertEqual(received_data, expected_data)
