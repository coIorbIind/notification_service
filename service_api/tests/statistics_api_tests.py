from django.urls import reverse
from django.test import TestCase
from rest_framework import status

from service_api.models import Mailing, Client, Message
from service_api.serializers import MailingDetailStatisticSerializer, MailingListStatisticSerializer


class CRUDTests(TestCase):
    def setUp(self) -> None:
        self.mailing_1 = Mailing.objects.create(
            start_time="2020-08-07T19:00",
            text="text",
            filters={
                "operator_code": 900
            },
            stop_time="2020-08-07T20:00"
        )

        self.mailing_2 = Mailing.objects.create(
            start_time="2020-08-07T20:00",
            text="text",
            filters={
                "operator_code": 900
            },
            stop_time="2020-08-07T21:00"
        )

        self.client_1 = Client.objects.create(
            phone_number="79009001020",
            operator_code=900,
            tag="tag",
            time_zone=3,
        )

        self.client_2 = Client.objects.create(
            phone_number="79009001030",
            operator_code=900,
            tag="tag",
            time_zone=2,
        )

        self.message_1 = Message.objects.create(
            client=self.client_1,
            mailing=self.mailing_1
        )

        self.message_2 = Message.objects.create(
            is_sent=True,
            client=self.client_2,
            mailing=self.mailing_1
        )

    def test_mailing_list_statistics(self):
        url = reverse('mailings-list-statistic')
        response = self.client.get(url)

        expected_data = MailingListStatisticSerializer([self.mailing_1, self.mailing_2], many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.data, expected_data)

    def test_mailing_detail_statistics(self):
        url = reverse('mailings-detail-statistic', args=(self.mailing_1.id,))
        response = self.client.get(url)

        expected_data = MailingDetailStatisticSerializer(self.mailing_1).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(response.data, expected_data)
