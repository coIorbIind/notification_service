import requests
from requests.exceptions import RequestException

from notification_service.settings import JWT_TOKEN, SEND_API_URL
from .models import Message


def send_message(message: Message) -> dict:
    headers = {
        "Authorization": f"Bearer {JWT_TOKEN}"
    }
    url = SEND_API_URL.format(msgID=message.id)
    try:
        response = requests.post(url=url, headers=headers, json={
            {
                "id": message.id,
                "phone": message.client.phone_number,
                "text": message.mailing.text
            }
        })
        result = {
            "status_code": response.status_code
        }
        if response.status_code == 400:
            result["response"] = "You are not authorized!"

        else:
            result["response"] = response.json()
            message.is_sent = True
            message.save()

    except RequestException:
        result = {
            "status_code": 400,
            "response": "Error during sending requests!"
        }

    return result
