from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.viewsets import ModelViewSet

from datetime import datetime
from django.utils import timezone

from .models import Client, Mailing, Message
from .serializers import (ClientSerializer, MailingSerializer, MailingListStatisticSerializer,
                          MailingDetailStatisticSerializer)
from .utils import send_message

from notification_service.wsgi import scheduler


class ClientViewSet(ModelViewSet):
    """
    API endpoints для обработки следующих запросов:
    post - добавление нового клиента в справочник
    delete - удаление клиента
    put/patch - обновление данных о клиенте
    """
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class MailingViewSet(ModelViewSet):
    """
    API endpoints для обработки следующих запросов:
    post - добавление новой рассылки
    delete - удаление рассылки
    put/patch - обновление атрибутов рассылки
    """
    model = Mailing
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        mailing = Mailing.objects.get(**serializer.data)

        start_time = mailing.start_time

        hour, minute, day, month, year = start_time.strftime("%H-%M-%d-%m-%Y").split("-")

        tag = mailing.filters.get("tag")
        operator_code = mailing.filters.get("operator_code")

        clients = set()

        if tag is None:
            pass
        else:
            clients |= set(Client.objects.filter(tag=tag))

        if operator_code is None:
            pass
        else:
            clients |= set(Client.objects.filter(operator_code=int(operator_code)))

        messages = list()

        for client in clients:
            messages.append(Message.objects.create(
                is_seen=False,
                mailing=mailing,
                client=client,
            ))

        if start_time > timezone.now():
            for message in messages:
                scheduler.add_job(send_message, trigger='cron', id=str(serializer.data.get("id")),
                                  year=year, month=month, day=day, hour=hour, minute=minute,
                                  kwargs={"message": message})

        else:
            for message in messages:
                send_message(message)

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class MailingListStatisticsView(ListAPIView):
    """
    API endpoint для получения общей статистики по всем рассылкам
    """
    queryset = Mailing.objects.all()
    serializer_class = MailingListStatisticSerializer


class MailingDetailStatisticsView(RetrieveAPIView):
    """
    API endpoint для получения статистики по конкретной рассылкам
    """
    queryset = Mailing.objects.all()
    serializer_class = MailingDetailStatisticSerializer
