from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import DateTimeField
from .models import Client, Mailing, Message


class ClientSerializer(ModelSerializer):
    """Сериализатор для класса Client"""
    class Meta:
        model = Client
        fields = "__all__"


class MailingSerializer(ModelSerializer):
    """Сериализатор для класса Mailing"""
    start_time = DateTimeField(format="%Y-%m-%dT%H:%M", input_formats=("%Y-%m-%dT%H:%M", ))
    stop_time = DateTimeField(format="%Y-%m-%dT%H:%M", input_formats=("%Y-%m-%dT%H:%M",))

    class Meta:
        model = Mailing
        fields = "__all__"


class MailingListStatisticSerializer(ModelSerializer):
    """Сериализатор для получения общей статистики для рассылок"""
    class Meta:
        model = Mailing
        fields = ("text", "all_messages_count", "sent_messages_count", "not_sent_messages_count")


class ClientShortSerializer(ModelSerializer):
    """Сериализатор с главной информацией о клиенте"""
    class Meta:
        model = Client
        fields = ("phone_number", )


class MessageSerializer(ModelSerializer):
    """Сериализатор для класса Message"""
    client = ClientShortSerializer()

    class Meta:
        model = Message
        fields = ("client", "is_sent")


class MailingDetailStatisticSerializer(ModelSerializer):
    """Сериализатор для получения детальной статистики по рассылке"""
    messages = MessageSerializer(many=True)

    class Meta:
        model = Mailing
        fields = ("text", "all_messages_count", "sent_messages_count", "not_sent_messages_count", "messages")
